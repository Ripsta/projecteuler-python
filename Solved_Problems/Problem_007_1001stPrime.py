from EulerMethods import isprime

i = 1
prime = []
while len(prime) < 10002:
    if isprime(i):
        prime.append(i)
        print(i)
    i = i + 1

print("The 10,001st prime is ", prime[10000])