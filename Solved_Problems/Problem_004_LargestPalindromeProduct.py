maximum, fact1, fact2 = 0, 0, 0
palindrome = []

for i in range (999999, 100000, -1):
    p = str(i)
    if p[0] in p[5] and p[1] in p[4] and p[2] in p[3]:
        palindrome.append(p)

for i in range ((len(palindrome)-1), 0, -1):
    for j in range (999, 100, -1):
        if int(palindrome[i]) % j == 0:
            x = int(int(palindrome[i])/j)
            f1, f2 = len(str(j)), len(str(x))

            if f1 == 3 and f2 == 3 :
                maximum, fact1, fact2 = palindrome[i], j, x

print(maximum, fact1, fact2)
