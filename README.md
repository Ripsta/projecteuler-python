# Project Euler Challenges with Python

The [Project Euler](https://projecteuler.net/) is a website with challenges that involve the use of both mathematical and numerical methods. The challenges in this repository are solved using Python.

**WARNING** These might not be (most likely won't be) the most efficient solutions. Just my solutions.


## Getting Started

Just run in any IDE of your choice.

## Challenges Solved

1. Multiples of 3 and 5
1. Even Fibonacci Numbers
1. Largest Prime Factor
1. Largest Palindrome Product

## Built With

* [Project Euler](https://projecteuler.net/)
* [Python](https://www.python.org/)


## Authors

* [Antonio J Suarez](https://gitlab.com/ouranophobia)
